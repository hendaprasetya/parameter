/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.param;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author KFC SOLUTION
 */
public class ParamPayment {

    String url = "";
    String billNo = "";
    ParamOutlet outlet;
    String totalAmount = "0";
    String totalTax = "0";
    Integer totalQty = 0;
    String totalSales = "0";
    String totalCharge = "0";
    String approvalCode = "";
    List<ParamOrderItem> listItem = new ArrayList<>();

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public ParamOutlet getOutlet() {
        return outlet;
    }

    public void setOutlet(ParamOutlet outlet) {
        this.outlet = outlet;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

    public Integer getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(Integer totalQty) {
        this.totalQty = totalQty;
    }

    public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String totalSales) {
        this.totalSales = totalSales;
    }

    public String getTotalCharge() {
        return totalCharge;
    }

    public void setTotalCharge(String totalCharge) {
        this.totalCharge = totalCharge;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public List<ParamOrderItem> getListItem() {
        return listItem;
    }

    public void setListItem(List<ParamOrderItem> listItem) {
        this.listItem = listItem;
    }
}
