/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

/**
 *
 * @author KFC SOLUTION
 */
public class HttpUtility {

    public String postResponse(String url, String param) {
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            final HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
            httpClient = new DefaultHttpClient(httpParams);
            HttpPost post = new HttpPost(url);
            System.out.println(" PARAM = " + param);
            StringEntity postingString = new StringEntity(param); //gson.tojson() converts your pojo to json
            post.setEntity(postingString);
            post.setHeader("Content-type", "application/json");
            HttpResponse rsp = httpClient.execute(post);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            (rsp.getEntity().getContent())
                    )
            );

            StringBuilder content = new StringBuilder();
            String line;
            while (null != (line = br.readLine())) {
                content.append(line);
            }
            return content.toString();

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(HttpUtility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HttpUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public String getResponse(String url, String param) {
        try {
            System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
            HttpClient httpClient = HttpClientBuilder.create().build();
            final HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
            httpClient = new DefaultHttpClient(httpParams);
            HttpGet get = new HttpGet(url.concat(param));
            System.out.println(" PARAM = " + param);
            get.setHeader("Content-type", "application/json");
            HttpResponse rsp = httpClient.execute(get);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            (rsp.getEntity().getContent())
                    )
            );

            StringBuilder content = new StringBuilder();
            String line;
            while (null != (line = br.readLine())) {
                content.append(line);
            }
            return content.toString();

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(HttpUtility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HttpUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
}
